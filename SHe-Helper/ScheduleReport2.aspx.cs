﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SHe_Helper
{
    public partial class ScheduleReport2 : System.Web.UI.Page
    {
        DBCON a = new DBCON();
        int sum=0;
        private void Page_Init(object sender, EventArgs e)
        {
            GetReport();
        }
        ReportDocument crystalReport = new ReportDocument();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        private void GetReport()
        {
            scheduleRpt2 objRpt = new scheduleRpt2();
            crystalReport = objRpt;
            a.ExcecuteQuery("select * from AgentTable t1 inner join DepositorAtable t2 on t1.Agent_id=t2.D_agentid inner join DepositorPtable t3 on t2.CIF=t3.CIF inner join DepositorLtable t4 on t2.CIF=t4.CIF where t1.Agent_id='" + Session["Agent"].ToString() + "' and t2.schedule_no='"+Request.QueryString["no"].ToString()+"'");
            for (int i = 0; i < a.DT.Rows.Count; i++)
            {
                sum = sum + Convert.ToInt32(a.DT.Rows[i]["D_denomination"].ToString());
            }
            crystalReport.SetParameterValue("sum", sum);
            crystalReport.SetDataSource(a.DT);
            CrystalReportViewer1.ReportSource = crystalReport;
           
            ShowPdf(crystalReport);
        }
        protected void ShowPdf(ReportDocument rptDocument)
        {
            try
            {

                DiskFileDestinationOptions DiskOpts = new DiskFileDestinationOptions(); // Document Cache Constructed in memory
                string path = Server.MapPath("~/Files") + @"\";           // Make Document Create Pathe=
                string Filename = System.Guid.NewGuid().ToString() + Convert.ToString(DateTime.Now.Second) + Convert.ToString(DateTime.Now.Millisecond); // Docment Name and Extension
                rptDocument.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile; // Saving Option


                Filename = Filename + ".pdf";
                rptDocument.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;

                DiskOpts.DiskFileName = path + Filename; // Combination of Path and FileName
                rptDocument.ExportOptions.DestinationOptions = DiskOpts;

                rptDocument.Export();

                {
                    Response.Redirect(ResolveUrl("~/Files/") + Filename, true);
                    //ScriptManager.RegisterStartupScript(Page, typeof(Page), "myscript", "window.open('" + ResolveUrl("~/Files/") + Filename + "',target='new');", true);
                }


            }
            catch (Exception ex)
            {


            }
        }
    }
}