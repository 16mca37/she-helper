﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SHe_Helper
{
    public partial class MonthlyReport : System.Web.UI.Page
    {
        ReportDocument crystalReport = new ReportDocument();
        Agent agent = new Agent();
        DBCON a = new DBCON();
        int totalaccounts;
        int totalamounts;
        int advance;
        int finaltot;
        int defAmt;
        int loan;
        int schedules;
        string date = DateTime.Now.Month.ToString();
        double allowance, commission;
        
        private void Page_Init(object sender, EventArgs e)
        {
            GetReport();
        }


        protected void Page_Load(object sender, EventArgs e)
        {
          
        }

        private void GetReport()
        {
            monthlyRpt objRpt = new monthlyRpt();
            crystalReport = objRpt;

            a.ExcecuteQuery("select sum(depositors) from ScheduleTable where agent_id='" + Session["Agent"].ToString() + "'");
            if (a.DT.Rows.Count != 0)
            {
                totalaccounts = Convert.ToInt32(a.DT.Rows[0][0].ToString());
            }

            a.DT.Clear();
            a.ExcecuteQuery("select sum(total) from ScheduleTable where agent_id='" + Session["Agent"].ToString() + "'");
            if (a.DT.Rows.Count != 0)
            {
                totalamounts = Convert.ToInt32(a.DT.Rows[0][0].ToString());
            }

            a.DT.Clear();
            a.ExcecuteQuery("select sum(advance) from ScheduleTable where agent_id='" + Session["Agent"].ToString() + "'");
            if (a.DT.Rows.Count != 0)
            {
                advance = Convert.ToInt32(a.DT.Rows[0][0].ToString());
            }

            finaltot = totalamounts + advance;

            a.DT.Clear();
            a.ExcecuteQuery("select sum(fine) from ScheduleTable where agent_id='" + Session["Agent"].ToString() + "'");
            if (a.DT.Rows.Count != 0)
            {
                defAmt = Convert.ToInt32(a.DT.Rows[0][0].ToString());
            }

            a.DT.Clear();
            a.ExcecuteQuery("select sum(loan) from ScheduleTable where agent_id='" + Session["Agent"].ToString() + "'");
            if (a.DT.Rows.Count != 0)
            {
                loan = Convert.ToInt32(a.DT.Rows[0][0].ToString());
            }

            a.DT.Clear();
            a.ExcecuteQuery("select count(*) from ScheduleTable where agent_id='" + Session["Agent"].ToString() + "'");
            if (a.DT.Rows.Count != 0)
            {
                schedules = Convert.ToInt32(a.DT.Rows[0][0].ToString());
            }

            agent.calculateSalary(Session["Agent"].ToString());
            commission = agent.effectiveCommision;
            allowance = agent.effectiveCommision;


            crystalReport.SetParameterValue("TotalAcc", totalaccounts);
            crystalReport.SetParameterValue("TotalAmt", totalamounts);
            crystalReport.SetParameterValue("advance", advance);
            crystalReport.SetParameterValue("DefaultAmt", defAmt);
            crystalReport.SetParameterValue("TotAmt", finaltot);
            crystalReport.SetParameterValue("Loan", loan);
            crystalReport.SetParameterValue("month",date);
            crystalReport.SetParameterValue("schedules", schedules);
            crystalReport.SetParameterValue("Allowance", allowance);
            crystalReport.SetParameterValue("Commission", commission);

            CrystalReportViewer1.ReportSource = crystalReport;

            ShowPdf(crystalReport);
        }
        protected void ShowPdf(ReportDocument rptDocument)
        {
            try
            {

                DiskFileDestinationOptions DiskOpts = new DiskFileDestinationOptions(); // Document Cache Constructed in memory
                string path = Server.MapPath("~/Files") + @"\";           // Make Document Create Pathe=
                string Filename = System.Guid.NewGuid().ToString() + Convert.ToString(DateTime.Now.Second) + Convert.ToString(DateTime.Now.Millisecond); // Docment Name and Extension
                rptDocument.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile; // Saving Option


                Filename = Filename + ".pdf";
                rptDocument.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;

                DiskOpts.DiskFileName = path + Filename; // Combination of Path and FileName
                rptDocument.ExportOptions.DestinationOptions = DiskOpts;

                rptDocument.Export();

                {
                    Response.Redirect(ResolveUrl("~/Files/") + Filename, true);
                    //ScriptManager.RegisterStartupScript(Page, typeof(Page), "myscript", "window.open('" + ResolveUrl("~/Files/") + Filename + "',target='new');", true);
                }


            }
            catch (Exception ex)
            {


            }
        }
    }
}