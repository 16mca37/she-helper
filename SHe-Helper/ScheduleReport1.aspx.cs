﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace SHe_Helper
{
    public partial class ScheduleReport1 : System.Web.UI.Page
    {
        DBCON a = new DBCON();
        private void Page_Init(object sender, EventArgs e)
        {
            GetReport();
        }
        ReportDocument crystalReport = new ReportDocument();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void GetReport()
        {
            a.ExcecuteQuery("select * from ScheduleTable t1 inner join DepositorAtable t2 on t1.agent_id=t2.D_agentid where t1.agent_id='" + Session["Agent"].ToString() + "'");
            scheduleRep1 objRpt = new scheduleRep1();
            crystalReport = objRpt;
            crystalReport.SetDataSource(a.DT);
            CrystalReportViewer1.ReportSource = crystalReport;
        }
        protected void ShowPdf(ReportDocument rptDocument)
        {
            try
            {

                DiskFileDestinationOptions DiskOpts = new DiskFileDestinationOptions(); // Document Cache Constructed in memory
                string path = Server.MapPath("~/Files") + @"\";           // Make Document Create Pathe=
                string Filename = System.Guid.NewGuid().ToString() + Convert.ToString(DateTime.Now.Second) + Convert.ToString(DateTime.Now.Millisecond); // Docment Name and Extension
                rptDocument.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile; // Saving Option


                Filename = Filename + ".pdf";
                rptDocument.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;

                DiskOpts.DiskFileName = path + Filename; // Combination of Path and FileName
                rptDocument.ExportOptions.DestinationOptions = DiskOpts;

                rptDocument.Export();

                {
                    Response.Redirect(ResolveUrl("~/Files/") + Filename, true);
                    //ScriptManager.RegisterStartupScript(Page, typeof(Page), "myscript", "window.open('" + ResolveUrl("~/Files/") + Filename + "',target='new');", true);
                }


            }
            catch (Exception ex)
            {


            }
        }
    }
}